import 'dart:convert';
import 'package:http/http.dart' as http;

class NetworkHelper{

  NetworkHelper(this.url);

  final String url;

  Future getData() async{
    http.Response weatherData = await http.get(url);
    if(weatherData.statusCode == 200) {
      String data = weatherData.body;
      return jsonDecode(data);
    }
    else{
      print(weatherData.statusCode);
    }
  }
}